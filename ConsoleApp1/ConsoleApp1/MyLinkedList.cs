﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class MyLinkedList<T>
    {
        public int n = 6;
        public T[] value;
        public int[] next;
        public int[] prev;
        int free;

        public MyLinkedList()
        {
            value = new T[n];
            next = new int[n];
            prev = new int[n];
            value[0] = default(T);
            next[0] = 0;
            prev[0] = 0;
            free = 1;
            for (int i = 1;n - 1 > i;i++)
                next[i] = i + 1;
            next[n - 1] = - 1;
        }
        private int Resize()
        {
            if (free == - 1)
            {
                n = n + 6;
                Array.Resize(ref value, n);
                Array.Resize(ref next, n);
                Array.Resize(ref prev, n);
                for (int i = n - 6; n - 1 > i; i++)
                    next[i] = i + 1;
                next[n - 1] = - 1;
                free = n - 6;
            }
            int q = free;
            free = next[q];
            return q;
        }
        public void AddLast(T name)
        {
            //int q = free;
            //free = next[q];
            int q = Resize();
            value[q] = name;
            next[q] = 0;
            next[prev[0]] = q;
            prev[q] = prev[0];
            prev[0] = q;
        }
        public void AddFirst(T name)
        {
            //int q = free;
            //free = next[q];
            int q = Resize();
            value[q] = name;
            prev[q] = 0;
            prev[next[0]] = q;
            next[q] = next[0];
            next[0] = q;
        }
        public void Insert(int p, T name)
        {
            //int q = free;
            //free = next[q];
            int q = Resize();
            value[q] = name;
            int r = next[p];
            next[q] = r;
            prev[q] = p;
            prev[r] = q;
            next[p] = prev[r];
        }
        public void Remove(int p)
        {
            value[p] = default(T);
            int q = prev[p];
            int r = next[p];
            next[q] = r;
            prev[r] = q;
            next[p] = free;
            free = p;
            prev[p] = 0;
        }
        public T Last()
        {
            return value[prev[0]];
        }
        public T First()
        {
            return value[next[0]];
        }
        public void Clear()
        {
            free = 1;
            next[0] = 0;
            prev[0] = 0;
            for (int i = 1; n - 1 > i; i++)
            {
                value[i] = default(T);
                next[i] = i + 1;
                prev[i] = 0;
            }
            next[n - 1] = -1;
        }
        public int Size()
        {
            int size = 0;
            int r = 0;
            if (next[0] != 0 && prev[0] != 0)
            {
                do
                {
                    size++;
                    r = next[r];
                }
                while (next[r] != 0);
            }
            return size;
        }
        public int IndexOf(T name)
        {
            int t = -1;
            int r = 0;
            if (next[0] != 0 && prev[0] != 0)
            {
                do
                {
                    if (Convert.ToString(name) == Convert.ToString(value[r]))
                    {
                        t = r;
                        break;
                    }
                    r = next[r];
                }
                while (next[r] != 0);
            }
            return t;
        }
        public string ShowList()
        {
            string str = "Empty";
            int r = 0;
            if (next[0] != 0 && prev[0] != 0)
            {
                str = "";
                do
                {
                    r = next[r];
                    str += Convert.ToString(value[r]) + " ";
                }
                while (next[r] != 0);
            }
            return str;
        }
    }
}
